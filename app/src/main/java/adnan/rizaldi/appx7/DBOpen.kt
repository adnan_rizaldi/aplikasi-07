package adnan.rizaldi.appx7

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpen(context: Context) : SQLiteOpenHelper (context,DB_Name,null,DB_Ver) {
    override fun onCreate(db: SQLiteDatabase?) {
        val Mahasiswa = "create table Mahasiswa(nim text primary key,Mhs text,prodi text)"
        db?.execSQL(Mahasiswa)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }

    companion object{
        val DB_Name= "Appx7"
        val DB_Ver= 1
    }
}