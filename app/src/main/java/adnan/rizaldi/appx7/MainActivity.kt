package adnan.rizaldi.appx7

import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.zxing.BarcodeFormat
import com.google.zxing.integration.android.IntentIntegrator
import com.google.zxing.integration.android.IntentResult
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnScanQR ->{
                //initiate a barcode scans and plays 'beep' sound when a barcode is detected
                intentIntegrator.setBeepEnabled(true).initiateScan()
            }
            R.id.btnGenerateQR ->{
                val barCodeEncoder = BarcodeEncoder()
                val bitmap = barCodeEncoder.encodeBitmap(edQrCode.text.toString(),
                BarcodeFormat.QR_CODE,400,400)
                imV.setImageBitmap(bitmap)
            }
            R.id.btnSimpan ->{
                dialog.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("No",null)
                dialog.show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val intentResult : IntentResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data)
        if(intentResult.contents != null){
            edQrCode.setText(intentResult.contents)
        }else{
            Toast.makeText(this,"Dibatalkan",Toast.LENGTH_SHORT).show()
        }
        val strToken = StringTokenizer(edQrCode.text.toString(),";", false)
        edNim.setText(strToken.nextToken())
        edMhs.setText(strToken.nextToken())
        edProdi.setText(strToken.nextToken())
        super.onActivityResult(requestCode, resultCode, data)
    }


    lateinit var lsAdapter : ListAdapter
    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var dialog : AlertDialog.Builder
    //IntentIntegrator is a part of zxing-android-embedded Library that is used to read QR Code
    lateinit var intentIntegrator: IntentIntegrator

    fun getDBObject():SQLiteDatabase{
        return db
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        db = DBOpen(this).writableDatabase
        dialog = AlertDialog.Builder(this)
        intentIntegrator = IntentIntegrator(this)
        btnGenerateQR.setOnClickListener(this)
        btnScanQR.setOnClickListener(this)
        btnSimpan.setOnClickListener(this)
    }

    fun showDataMhs(){
        var sql = ""
        sql = "select nim as _id, Mhs, prodi from Mahasiswa"
        val c : Cursor = db.rawQuery(sql,null)
        adapter = SimpleCursorAdapter(this,R.layout.item_data_mhs,c, arrayOf("_id","Mhs","prodi"),
        intArrayOf(R.id.txNim,R.id.txMhs,R.id.txProdi),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lvMhs.adapter = adapter
    }

    fun insertDataMhs(nim : String, mhs : String, prodi : String){
        var cv : ContentValues = ContentValues()
        cv.put("nim",nim)
        db.insert("Mahasiswa",null,cv)
        cv.put("Mhs",mhs)
        db.update("Mahasiswa",cv,"nim=$nim",null)
        cv.put("prodi",prodi)
        db.update("Mahasiswa",cv,"nim=$nim",null)
        edQrCode.setText("")
        edNim.setText("")
        edMhs.setText("")
        edProdi.setText("")
        showDataMhs()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataMhs(edNim.text.toString(),edMhs.text.toString(),edProdi.text.toString())
    }

    override fun onStart() {
        super.onStart()
        showDataMhs()
    }
}
